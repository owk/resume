Weikang Ou
=============

Software Developer
-----------------------

- Phone: [+86 17600515077](tel://8617600515077)
- Email: <kingflight123@gmail.com>

Summary
-------

I am an experienced software developer who has participated in and led innovative projects in different areas. I am passionate about developing algorithms to solve difficult problems in industry and life. Through being conversant in architecture designing, algorithm designing, Object-oriented programming, Event-driven programming, I am able to implement robust extensible software solution.

Specialties
-----------

Designing arhitecture for big piece of software. Developing algorithm intense software with C++, rust. Writing automated test cases, makefiles and API documents. Drawing from a strong background in many diverse technologies and frameworks.

Experience
----------

### **Technical Director** at [Leridge Technologies](http://leridge.com), Beijing

*August 2015 - July 2018* (3 years)

As a server-side web developer in the Web team, I developed features to support the garment selling platform on iOS, Android and Wap. Features including ordering, shopping cart and a recommendation system based on online questionnaires were developed by me independently on the platform of Yaf. I also developed some front end pages on wap like the shopping cart and the questionnaires using HTML5 and Javascript.

After the company transformed to focus on manufacture. I worked as member of the Solution Innovations team, being responsible for designing scheduling algorithms to support flexible and autonomous manufacturing in a robot-assisted garment factory. I play a dominant role in implementing most of modules in C++ and Rust and in developing the simulation program of the algorithm based on Unity3D. I also designed the communication system among the server, factory and the AGV robot system.

### **Software Engineer** at [Huawei Technologies](http://www.huawei.com), Wuhan

*December 2012 - August 2015* (3 years)

I designed and implemented a special feature which enables the NE devices in a WDM(Wavelength Division Multiplexing) optical network to discover the fiber connections automatically through analyzing the optical power at points with optical power detection ability. I also transplanted this feature from the product OSN1800 to OSN8800.

I implemented an algorithm namely eMCA designed by optics experts to support high definition optical SNR monitoring in OMS(Optical Multiplex Section) through reading the single wavelength powers in the first and last NE device of the OMS and putting these data as input to some special formulas.

I rewrited the OLS module to make it more readable, extensible and maintainable. The OLS module is to enables the NEs in a optical network to report alarm at a specific point according to the OTN protocol when a fault occurs.

I also participated in the QCC activities which is held in Huawei once a year through which I developed a tool that can find undesired configuration of a NE through reading the logs.


Skills & Expertise
------------------

These are languages, tools, and practices to which I have had exposure over the
past 6 years or so. Those things which enjoy routine usage in my daily work are
denoted with a * symbol.

### Programming Languages

- [C++]() *
- [Java]()
- [Python]()
- [RUST]()
- [C#]()
- [JavaScript](http://developer.mozilla.org/en/JavaScript)
- [Dart](https://www.dartlang.org/)
- [PHP](http://php.net)

### Markup/Templating Languages & Preprocessors

- [CSS](http://www.w3.org/Style/CSS/Overview.en.html) *
- [HTML](http://developers.whatwg.org) *
- [LESS](http://lesscss.org) *
- [Markdown](http://daringfireball.net/projects/markdown) *
- [Smarty](http://smarty.net)

### Frameworks & APIs

- [flutter](https://flutter.io/) *
- [Unity3D](https://unity3d.com/) *
- [jQuery](http://jquery.com) 
- [Node.js](http://nodejs.org) 
- [React](http://facebook.github.io/react)

### Software & Tools

- [Git](http://git-scm.com) *
- [Amazon Web Services](http://aws.amazon.com)
- [Nginx](http://wiki.nginx.org) *
- [Apache](http://apache.org)
- [Apple XCode](http://developer.apple.com)
- [Mac OS X](http://apple.com/macosx) *
- [MongoDB](http://mongodb.org)
- [MySQL](http://mysql.com) *
- [PostgreSQL](http://postgresql.org)
- [Sublime Text](http://www.sublimetext.com)
- [Subversion](http://svn.apache.org)
- [Ubuntu Linux](http://ubuntu.com)
- [Vim](http://www.vim.org) *
- [VirtualBox](http://virtualbox.org)
- [WebStorm](http://jetbrains.com/webstorm)
- [VS code](https://code.visualstudio.com/) *
- [GCC](https://gcc.gnu.org/) *

Education
---------

B.S., Electrical Information Science and Technology, [Wuhan University of Technology](http://whut.edu.cn), 2008 - 2012

Certificates
---------
- [TOFEL](https://www.ets.org/toefl) mark: 93
- [CET6](http://cet.etest.net.cn/) mark: 616
- [National Computer Rank Examination](http://ncre.neea.edu.cn/) Rank 2, C++ Programming
- [National Computer Rank Examination](http://ncre.neea.edu.cn/) Rank 3, Computer Network
- [National Computer Rank Examination](http://ncre.neea.edu.cn/) Rank 4, Database

Language
---------
- English
- Chinese mandarin
- Cantonese

Interests
---------

- Swimming
- Listening to music
- Watching and contributing to open source software

©2018 Weikang Ou. All rights reserved.

